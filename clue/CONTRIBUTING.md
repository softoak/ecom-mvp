# Contributing

- **[Git Workflow](#git-workflow)**
- **[Commits Format](#commits-format)**
- **[Code Review Methodology](#code-review-methodology)**
- **[Merge Requests](#merge-requests)**
- **[Release Process](#release-process)**

You must be added as a developer in your project in order to be able to start working. This is usually automatic before your come but if it is not, that may leads you to permissions issues.

Your regular daily basis workflow will look like the following :

- **master** – branch that corresponds to the current production code
- **develop** – development integration branch – latest integrated development code
- **feat/123-some-text** – individual feature-branch being developed by developer where 123 is change/issue number
- **fix/123-some-text** – individual bugfix-branch being developed by developer where 123 is change/issue number
- **release/1.0.1** – release candidate branch being tested and under feature-freeze
- **fix/123-text** – hotfix branch being developed where 123 is change/issue number

In the context of this MVP I've pushed dirrectly in develop which is not allowed in a usual work case because it will break the process of review and validation and will most likely break code.

**[Back to top](#contributing)**

## Git workflow

You must ensure that your branch could be merged in develop branch without conflicts.

In order to do this you need to have your local develop branch up to date.

```sh
git checkout develop
git pull --rebase
```

Go back to your branch and rebase it with the changes from develop

```sh
git checkout feature/ticketnumber-description
git rebase develop
```

In case of conflicts resolve them and run:

_Follow instructions displayed in your terminal by the git rebase command if conflicts_

```sh
git add .
```

```sh
git rebase --continue
```

Push your code to the remote repository:

```sh
git push -f origin feature/ticketnumber-description
```

**[Back to top](#contributing)**

## Commits Format

Contribution guidelines for changelog with `standard-version`
Why is it important? Commit messages end up in application's changelog. 
They help you identify changes made in each commit, thus making your debugging (when needed) easier/possible.

## Desired commit message pattern:
> `keyword(scope): JIRA-TASK - short description of what you did`
OR
> `keyword(scope): short description of what you did JIRA-TASK`


You can [include emoji](https://gitmoji.dev/) in your commit description if you want to be more descriptive, this is not mandatory.

## Code Review Methodology

When reviewing an MR consider using 👀 emoji to let know that your are reviewing and 👍 to say that this MR looks good to you.


**[Back to top](#contributing)**

## Merge Requests

To be validated and merged, a merge request must be approved by 2 other frontend developers

### RULES

**List of approvers**
- Be careful to be aware of the business part
- 1 dev and 1 QA
  - What about refactoring and technical update
  - It’s a friendly rules not blocking in gitlab
- Can be from another squad
- Add people on the MR reviewer list 
- If you actively review add you in Assignee list

**Comment**
- Put in the title if your comment is blocking / question / suggestion 

### STEPS

**Step 1:**

 - Merge request must be reviewed by one developers and one QA
this is a friendly rule, the system will not block you since you have 2 approves

**Step 2:**

- Resolve code review discussions

**Step 3:**

- Check the box "Squash your commit" in GitLab Merge Request (or do it while rebasing). The title of the MR must respect the conventional commit format 


- If the code you want to merge was develop by several developers please use [co-authoring]

**Step 4:**

- Merge Request accepted
- merged by **the author** of the MR. Although there will be exceptions and the code author will not be able to do it, so in those cases the reviewer can/should do it.
- Be careful with the previous rules
- Be careful to leave a message to each comment before merge

**[Back to top](# contributing)**

## Release Process
After merging a number of feature branches fo `develop`, we create a `release` branch with a proper version. The moment we do that, `release/x.x.x` and `develop` branches start living their separate lives (just like forks).
By the time `release/x.x.x` is merged to `master`, and then, changes are rebased onto `develop`, the `develop` branch collected new contributions, `release/x.x.x` might have received hotfixes - both would lead to conflicts during the rebase.
Conflicts during the rebase will cause every merge request in review to have conflicts as well, and further, costly conflicts resolution and time spent waiting for pipelines to complete.

Problems highlighted for the above can be remedied by shortening the time `release/x.x.x` and `develop` act like forks.
After creating `release/x.x.x`, bumping the version and generating new changelog, we merge.

[Go Back](/README.md)
