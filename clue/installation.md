# Installation

## Build Setup

This project is running in version 18.14.0.
We will always keep in mind that NodeJs releases keep coming and we have to always stay under an LTS version.
You need the appropriate version of Node to build and run the project.
Otherwise, you can use `nvm` which is a library allowing  you to install and manage different NodeJs versions.
`nvm install` and it will install the right one: 

```bash
nvm list
```
This shows you the NodeJs version you currently have

```bash
nvm install
```
to install the needed version

 if you do not have NVM, you can install NVM or install Node 18.14.0 manually.


## Serve project locally - modes

```bash
# serve with hot reload at localhost:4200
$ ng serve

# build project into a dist folder
$ ng build

```

## Lints and fixes files

```bash
ng lint
```

## Tests

### Run unit tests

```bash
$ ng test

# run specific file tests
$ ng test --include='path/to/your/file'

```

## Performances

### Lighthouse

Lighthouse is an open-source, automated tool for improving the quality of web pages. You can run it against any web page, public or requiring authentication. It has audits for performance, accessibility, progressive web apps, SEO and more.

## Prepare release

### Lerna
To upgrade the version automatically in package.json one can use standard-version. It will do the following:

- bump the version in the package.json based on the commits and the Semantic versioning
- generate a changelog based on the commits (uses conventional-changelog under the hood) from the last git tag. This latter should be prefixed with 'v'.
- create a new commit including the package.json and updated CHANGELOG (with the commit message chore(release): X.X.X)
- not create a new tag with the new version number (as this step is skipped)

## _Keywords tools used in this project_

- Angular
- NgMaterial
- Jasmin
- Typescript
- Scss
- ESLint

[Go Back](/README.md)
