# Third party dependencies

---
## Angular Material
Material Design components for Angular

[Homepage](https://material.angular.io/) - [Repository](https://github.com/angular/components) - [Issues](https://github.com/angular/components/issues) - License: MIT

Install with Angular schematic: `ng add @angular/material`<br/>

---

[Go Back](/README.md)
