# Project Architecture Overview

This document provides an overview of the architecture of our Angular project, highlighting the organization of modules and their responsibilities.
I have opted for the use of ngModules, Standalone components seems to growing concept, but from the official docs, the transition can be smooth as ngModules can interact with Standalone components with ease.

## Modules

### Core Module

The Core module serves as the foundation of our Angular application, providing essential services, utilities, and configurations that are used across the entire application. This module should contain the elements necessary for the booting of our application, so It's included in app.module. It typically includes:

- **Singleton Services:** Services that are used globally throughout the application, such as authentication, logging, error handling, and API communication services.
- **Data Models:** Model used by the services or components in the core Module
- **Interceptors:** HTTP interceptors for adding headers, handling errors, or modifying requests/responses globally. (Those would be added once we implement authorization)
- **Guards:** Route guards for controlling access to routes based on user authentication and permissions. (Used for authentication and path protection)

### Feature Modules

Feature modules encapsulate different features or functional areas of the application, grouping related components, services, and other resources together. Each feature module should be self-contained and independent, promoting modularity and reusability. Some common features include:

- **Products Module:** Manages everything related to product listings, details, and management.
- **Shopping Cart Module:** Handles shopping cart functionality, including adding/removing items, updating quantities.

Feature modules typically consist of:

- **Components:** UI components specific to the feature.
- **Services:** Feature-specific services for data retrieval, manipulation, and interaction with APIs.
- **Routing Configuration:** Route definitions for the feature, including lazy loading for optimal performance. (having a route for /shopping-cart was planned but the use of menu was opted for; although we can still have a route that opens the menu)

### Shared Module

The Shared module contains components, directives, pipes, and other elements that are shared and reused across multiple feature modules. It promotes code reuse and consistency throughout the application. Common elements found in the Shared module include:

- **Shared Components:** Reusable UI components such as productsCard component to be reused across our app.
- **Directives:** Custom directives for DOM manipulation or behavior modification.
- **Pipes:** Custom pipes for data transformation or formatting.
- **Shared Services:** Services that are shared across multiple feature modules but don't belong to the Core module.

## Module Interactions

- **Core-Feature Interaction:** Feature modules can depend on services provided by the Core module for authentication, logging, API communication, etc.
- **Feature-Feature Interaction:** Feature modules can communicate with each other through shared services or events using observables or other patterns.
- **Shared Module Usage:** Both Core and Feature modules can utilize components, directives, and pipes provided by the Shared module for consistency and code reuse.

## Lazy loading
One major benefit of this architecture is making our application lightly coupled and scalable, it can keep growing, we will have those 3 pillars.
We can decide which module to import for which route (/products loads only that specific chunk, etc)

## DDD
Domain Driven Development is a strong patters enabling further code organization and separation of concern. I've added 3 aliases for our 3 parts to simplify imports and inclusion.

>**Important:** Avoid importing using alias for importations within the same Domain as it might lead to circular dependencies, instead use relative paths.

## Theming and SCSS

A great way to keep our application theme compliant and add more control into our styling, We could create our own theme with variables(color palette, breakpoints, font sizing, spacing, etc)
And ensure that those variables with those standard values are used instead of any static values


## CSS naming convention

The project use the CSS convention BEM (Block Element Modifier).

The BEM approach ensures that everyone who participates in the development of a website works with a single codebase and speaks the same language. Using proper naming will prepare you for the changes in design of the website.

### class name template in BEM:

We use [BEM naming convention](http://getbem.com/naming/)

```.block__elem--mod```

#### HTML:

```
<form class="form form--theme-xmas form--simple">
  <input class="form__input" type="text" />
  <input
    class="form__submit form__submit--disabled"
    type="submit" />
</form>
```

#### SCSS:

```
.form {

    &--theme-xmas { }

    &--simple { }

    &__input { }

    &__submit {

        &--disabled { }
    }
}
```

## Conclusion

By organizing our Angular application into Core, Feature, and Shared modules, we promote modularity, reusability, and maintainability. Each module has a clear responsibility and helps to keep our codebase organized and scalable.


[Go Back](/README.md)
