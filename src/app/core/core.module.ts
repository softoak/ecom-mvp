import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ShoppingCartModule } from '@features/shopping-cart/shopping-cart.module';
import { EcomHeaderComponent } from './components/ecom-header/ecom-header.component';

@NgModule({
  declarations: [
    EcomHeaderComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ShoppingCartModule,
    MatBadgeModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MatToolbarModule
  ],
  exports: [
    EcomHeaderComponent
  ]
})
export class CoreModule { }
