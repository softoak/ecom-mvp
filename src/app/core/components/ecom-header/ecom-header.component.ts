import { Component, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';
import { CartService } from '@core/services/cart.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { CartItem } from '@core/models/CartItem';

@Component({
  selector: 'app-ecom-header',
  templateUrl: './ecom-header.component.html',
  styleUrl: './ecom-header.component.scss'
})
export class EcomHeaderComponent implements OnDestroy {
  productsCount = 0;
  totalPrice = 0;
  cartItems: CartItem[] = [];
  private productsCountSubscription: Subscription;
  private cartItemsSubscription: Subscription;

  constructor(private cartService: CartService) {
    this.productsCountSubscription = this.cartService.productsCountSubject.subscribe(count => {
      this.productsCount = count;
    });
    this.cartItemsSubscription = this.cartService.cartItemsSubject.subscribe(newCartItems => {
      this.cartItems = newCartItems;
    });
  }

  calculateTotalPrice(): void {
    this.totalPrice = this.cartItems.reduce((total, item) => {
      return total + (item.price * (item.quantity ?? 0));
    }, 0);
  }

  openMenu(menuTrigger: MatMenuTrigger): void {
    // a subscriber could also be used here as an alternative, but might be costly
    this.cartItems = this.cartService.getCartItems();
    this.calculateTotalPrice();
    menuTrigger.openMenu();
  }

  handleIncreaseQuantity(id: number): void {
    this.cartService.increaseQuantity(id);
    this.calculateTotalPrice();
  }

  handleDecreaseQuantity(id: number): void {
    this.cartService.decreaseQuantity(id);
    this.calculateTotalPrice();
  }

  handleRemoveItem(id: number): void {
    this.cartService.removeFromCart(id);
    this.calculateTotalPrice();
  }

  ngOnDestroy(): void {
    this.productsCountSubscription.unsubscribe();
    this.cartItemsSubscription.unsubscribe();
  }
}
