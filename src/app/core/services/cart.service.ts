import { Injectable } from '@angular/core';
import { CartItem } from '../models/CartItem';
import { Subject } from 'rxjs';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private productsCount: number = 0;

  // mocking a store to persist data within the session's span
  private cartItems: { [key: string]: CartItem } = {};

  // don't use a behavioral Subject here
  productsCountSubject = new Subject<number>;
  cartItemsSubject = new Subject<CartItem[]>;

  constructor( private snackBar: MatSnackBar) { }

  getCartItems(): CartItem[] {
    return Object.values(this.cartItems);
  }

  getCartItem(id: number): CartItem {
    return this.cartItems[id];
  }

  addToCart(item: CartItem): void {
    const currentItem = item && this.cartItems[item.id];
    if (currentItem && currentItem.quantity) {
      this.cartItems[item.id].quantity! += 1;
    } else {
      this.cartItems[item.id] = { ...item, quantity: 1 };
      this.productsCount += 1;
      this.productsCountSubject.next(this.productsCount);
    }
    this.snackBar.open('Product added to cart', 'Dismiss', {
      duration: 2000,
      panelClass: ['custom-snackbar']
    });
  }

  decreaseQuantity(itemId: number): void {
    const cartItem = this.cartItems[itemId];
    if(cartItem && cartItem.quantity && cartItem.quantity > 0){
      cartItem.quantity -= 1;
    }
  }

  increaseQuantity(itemId: number): void {
    if(this.cartItems[itemId]){
      this.cartItems[itemId].quantity! += 1;
    }
  }

  removeFromCart(itemId: number): void {
    if(this.cartItems[itemId]) {
      delete this.cartItems[itemId];
      this.productsCount -= 1;
      this.productsCountSubject.next(this.productsCount);
      this.cartItemsSubject.next(this.getCartItems());
    }
  }

  clearCart(): void {
    this.cartItems = {};
  }
}
