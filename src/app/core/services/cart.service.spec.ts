import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { CartService } from './cart.service';
import { cartItemsStoreMock } from '../mocks/cart.service.mock';

describe('GIVEN CartService', () => {
  let service: CartService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
    });
    service = TestBed.inject(CartService);
  });
  afterEach(() => {
    TestBed.resetTestingModule();
  });

  it('ThHEN service should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('WHEN getCartItem is called', () => {
    describe('AND store contains cart items', () => {
      beforeEach(() => {
        cartItemsStoreMock.forEach((cartItem) => {
          service.addToCart(cartItem);
        });
      });
      it('THEN found cart item should be returned for existing id', () => {
        const mockedCartItem = cartItemsStoreMock[0];
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem).toBeDefined();
      });
      it('THEN no cart item should be returned for nonexisting id', () => {
        const expectedCartItem = service.getCartItem(-1000);
        expect(expectedCartItem).toBeUndefined();
      });
      it('THEN clearCart should clear the store', () => {
        expect(service.getCartItems()).toBeDefined();
        service.clearCart();
        expect(service.getCartItems()).toEqual([]);
      });
    })
  })

  describe('WHEN addToCart is called', () => {
    describe('AND cart item already exists', () => {
      const mockedCartItem = cartItemsStoreMock[0];
      beforeEach(() => {
        service.addToCart(mockedCartItem);
      });
      it('Then the quantity should be increased', () => {
        service.addToCart(mockedCartItem);
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem.quantity).toBe(2);
      })
    })
    describe('AND cart item doesn\'t have the item', () => {
      const mockedCartItem = cartItemsStoreMock[0];
      it('Then the quantity should be set to 1', () => {
        service.addToCart(mockedCartItem);
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem.quantity).toBe(1);
      });
      it('AND productsCountSubject emits the increase', (done: DoneFn) => {
        service.productsCountSubject.subscribe(count => {
          expect(count).toEqual(1);
          done();
        });
        service.addToCart(mockedCartItem);
      });
    });
  });

  describe('WHEN decreaseQuantity is called', () => {
    const mockedCartItem = cartItemsStoreMock[0];
      beforeEach(() => {
        service.addToCart(mockedCartItem);
        service.addToCart(mockedCartItem);
        service.addToCart(mockedCartItem);
      });
      it('Then the quantity should be decreased', () => {
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem.quantity).toBe(3);
        service.decreaseQuantity(expectedCartItem.id);
        expect(expectedCartItem.quantity).toBe(2);
      })
  })

  describe('WHEN increaseQuantity is called', () => {
    const mockedCartItem = cartItemsStoreMock[0];
      beforeEach(() => {
        service.addToCart(mockedCartItem);
        service.addToCart(mockedCartItem);
        service.addToCart(mockedCartItem);
      });
      it('Then the quantity should be increased', () => {
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem.quantity).toBe(3);
        service.increaseQuantity(expectedCartItem.id);
        expect(expectedCartItem.quantity).toBe(4);
      })
  })

  describe('WHEN removeFromCart is called', () => {
    const mockedCartItem = cartItemsStoreMock[0];
      beforeEach(() => {
        service.addToCart(mockedCartItem);
      });
      it('Then the store should not have mockedCartItem', () => {
        const expectedCartItem = service.getCartItem(mockedCartItem.id);
        expect(expectedCartItem).toBeDefined();
        service.removeFromCart(expectedCartItem.id);
        expect(service.getCartItem(mockedCartItem.id)).toBeUndefined();
      })
  })
});
