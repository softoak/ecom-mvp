import { Component, Input } from '@angular/core';
import { Product } from '@features/products/models/Product';
import { CartService } from '@core/services/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrl: './product-card.component.scss'
})
export class ProductCardComponent {
  @Input() product!: Product;
  currency: string;

  constructor(private cartService: CartService) {
    this.currency = '€'; // this should be fetched from API depending on countries
  }

  addToCart(): void {
    this.cartService.addToCart(this.product);
  }
}
