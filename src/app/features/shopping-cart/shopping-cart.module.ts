import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';
import { ShoppingCartItemComponent } from './components/shopping-cart-item/shopping-cart-item.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    ShoppingCartItemComponent
  ],
  imports: [
    CommonModule,
    ShoppingCartRoutingModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    ShoppingCartItemComponent,
  ]
})
export class ShoppingCartModule { }
