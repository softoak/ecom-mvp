import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CartItem } from '@core/models/CartItem';

@Component({
  selector: 'app-shopping-cart-item',
  templateUrl: './shopping-cart-item.component.html',
  styleUrl: './shopping-cart-item.component.scss'
})
export class ShoppingCartItemComponent {
  @Input() cartMenuItem!: CartItem;

  @Output() increaseQuantity = new EventEmitter<number>();
  @Output() decreaseQuantity = new EventEmitter<number>();
  @Output() removeItem = new EventEmitter<number>();
}
