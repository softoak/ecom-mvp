import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { ProductsService } from './products.service';
import { products } from '../mocks/products.service.mock';

describe('ProductsService', () => {
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let productService: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    productService = new ProductsService(httpClientSpy);
  });

  it('should be created', () => {
    expect(productService).toBeTruthy();
  });

  describe('AND getProducts is called', () => {
    describe('AND HttpClient.get resolves', () => {
      beforeEach(() => {
        httpClientSpy.get.and.returnValue(of(products));
      });
      it('THEN it should call HttpClient once', () => {
        productService.getProducts();
        expect(httpClientSpy.get.calls.count()).withContext('one call').toBe(1);
      });
    });
    describe('AND HttpClient.get rejects', () => {
      beforeEach(() => {
        const errorResponse = new HttpErrorResponse({
          error: 'test 404 error',
          status: 404,
          statusText: 'Not Found',
        });
        httpClientSpy.get.and.returnValue(throwError(() => errorResponse));
      })
      it('should return an error when the server returns a 404', (done: DoneFn) => {
        productService.getProducts().subscribe({
          next: () => done.fail('expected an error, not products'),
          error: (error) => {
            expect(error.error).toContain('test 404 error');
            done();
          },
        });
      });
    })
  });
});
