import { Component } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Product } from '../../models/Product';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrl: './products-list.component.scss'
})
export class ProductsListComponent {
  products$: Observable<Product[]>;

  constructor(private productsService: ProductsService) {
    this.products$ = this.getProducts();
  }

  getProducts(): Observable<Product[]> {
    return this.productsService.getProducts()
  }
}
