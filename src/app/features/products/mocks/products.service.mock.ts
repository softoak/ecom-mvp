import { Product } from "../models/Product"
export const products: Product[] = [
  {
    id: 1,
    title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
    price: 109.95,
    image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
    description: 'test desc',
    category: 'test cat',
    rating: {
      count: 100,
      rate: 3,
    }
  },
  {
    id: 2,
    title: 'Mens Casual Premium Slim Fit T-Shirts ',
    price: 22.3,
    image: 'https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg',
    description: 'test desc2',
    category: 'test cat2',
    rating: {
      count: 100,
      rate: 3,
    }
  },
  {
    id: 3,
    title: 'Mens Cotton Jacket',
    price: 55.99,
    image: 'https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg',
    description: 'test desc3',
    category: 'test cat3',
    rating: {
      count: 100,
      rate: 3,
    }
  },
  {
    id: 4,
    title: 'Mens Casual Slim Fit',
    price: 15.99,
    image: 'https://fakestoreapi.com/img/71YXzeOuslL._AC_UY879_.jpg',
    description: 'test des5',
    category: 'test cat5',
    rating: {
      count: 100,
      rate: 3,
    }
  },
];

export const productMock: Product = {
  id: 1,
  title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
  price: 109.95,
  image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
  description: 'test desc',
  category: 'test cat',
  rating: {
    count: 100,
    rate: 3,
  }
};
